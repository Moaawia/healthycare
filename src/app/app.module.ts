import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from "@angular/http";
import { AppRoutingModule } from './app-routing.module';
import {NgxPaginationModule} from 'ngx-pagination';
import { AppComponent } from './app.component';
import { ShowStatisticsComponent } from './system/employee/responsible/operations/show-statistics/show-statistics.component';
import { ViewResultComponent } from './system/employee/responsible/operations/view-result/view-result.component';
import { AddVaccineComponent } from './system/employee/responsible/operations/add-vaccine/add-vaccine.component';
import { AddQuestionnaireComponent } from './system/employee/responsible/operations/add-questionnaire/add-questionnaire.component';
import { ManageArticalesComponent } from './system/employee/responsible/operations/manage-articales/manage-articales.component';
import { ViewArticleComponent } from './system/employee/responsible/operations/view-article/view-article.component';
import { AddRecordComponent } from './system/employee/emp/operations/add-record/add-record.component';
import { EditeRecordComponent } from './system/employee/emp/operations/edite-record/edite-record.component';

import { BlockUserComponent } from './system/employee/manager/operations/block-user/block-user.component';
import { MainOperationsResponsibleComponent } from './system/employee/responsible/operations/main-operations-responsible/main-operations-responsible.component';
import { MainOperationsEmpComponent } from './system/employee/emp/operations/main-operations-emp/main-operations-emp.component';
import { MainOperationsManagerComponent } from './system/employee/manager/operations/main-operations-manager/main-operations-manager.component';
import { HomeComponent } from './system/home/home.component';
import { ManageArticalesOperationsComponent } from './system/employee/responsible/operations/manage-articales-operations/manage-articales-operations.component';
import { OperationsService } from './service/operations.service';
import { AddArticlesComponent } from './system/employee/responsible/operations/add-articles/add-articles.component';
import { DeleteArticlesComponent } from './system/employee/responsible/operations/delete-articles/delete-articles.component';
import { EditArticlesComponent } from './system/employee/responsible/operations/edit-articles/edit-articles.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './system/login/login.component';
import { AuthGuardAdminService } from './service/auth-guard-admin.service';
import { AuthService } from './service/auth.service';

import { RegisterResponsibleComponent } from './system/employee/manager/operations/register-responsible/register-responsible.component';
import { RegisterEmpComponent } from './system/employee/manager/operations/register-emp/register-emp.component';
import { GetUsersComponent } from './system/employee/manager/operations/get-users/get-users.component';
import { GetUserComponent } from './system/employee/manager/operations/get-user/get-user.component';
import { DeleteUserComponent } from './system/employee/manager/operations/delete-user/delete-user.component';
import { UpdateUserInfoComponent } from './system/employee/manager/operations/update-user-info/update-user-info.component';
import { GitUserInfoComponent } from './system/employee/emp/operations/git-user-info/git-user-info.component';
import { RegisterUserComponent } from './system/employee/emp/operations/register-user/register-user.component';
import { UpdateUserInfoEmpComponent } from './system/employee/emp/operations/update-user-info-emp/update-user-info-emp.component';
import { GetAllDoctorComponent } from './system/employee/responsible/operations/get-all-doctor/get-all-doctor.component';
import { ManagerDoctorComponent } from './system/employee/responsible/operations/manager-doctor/manager-doctor.component';
import { AddDoctorComponent } from './system/employee/responsible/operations/add-doctor/add-doctor.component';
import { EditDoctorComponent } from './system/employee/responsible/operations/edit-doctor/edit-doctor.component';
import { DeleteDoctorComponent } from './system/employee/responsible/operations/delete-doctor/delete-doctor.component';
import { GetUserInfoComponent } from './system/employee/responsible/operations/get-user-info/get-user-info.component';
import { UpdateUserInfoResComponent } from './system/employee/responsible/operations/update-user-info-res/update-user-info-res.component';
import { ManagerQuestionnaireComponent } from './system/employee/responsible/operations/manager-questionnaire/manager-questionnaire.component';
import { UpdateQuestionnaireComponent } from './system/employee/responsible/operations/update-questionnaire/update-questionnaire.component';
import { ViewQuestionnaireComponent } from './system/employee/responsible/operations/view-questionnaire/view-questionnaire.component';
import { GitAllUserComponent } from './system/employee/emp/operations/git-all-user/git-all-user.component';
import { ViewRecordComponent } from './system/employee/emp/operations/view-record/view-record.component';
import { ChartsModule } from 'ng2-charts';
import { AddVaccineEmpComponent } from './system/employee/emp/operations/add-vaccine-emp/add-vaccine-emp.component';
@NgModule({
  declarations: [
    AppComponent,
    ShowStatisticsComponent,
    ViewResultComponent,
    AddVaccineComponent,
    AddQuestionnaireComponent,
    ManageArticalesComponent,
    ViewArticleComponent,
    AddRecordComponent,
    EditeRecordComponent,
    
    BlockUserComponent,
    MainOperationsResponsibleComponent,
    MainOperationsEmpComponent,
    MainOperationsManagerComponent,
    HomeComponent,
    ManageArticalesOperationsComponent,
    AddArticlesComponent,
    DeleteArticlesComponent,
    EditArticlesComponent,
    LoginComponent,
    
    RegisterResponsibleComponent,
    RegisterEmpComponent,
    GetUsersComponent,
    GetUserComponent,
    DeleteUserComponent,
    UpdateUserInfoComponent,
    GitUserInfoComponent,
    RegisterUserComponent,
    UpdateUserInfoEmpComponent,
    GetAllDoctorComponent,
    ManagerDoctorComponent,
    AddDoctorComponent,
    EditDoctorComponent,
    DeleteDoctorComponent,
    GetUserInfoComponent,
    UpdateUserInfoResComponent,
    ManagerQuestionnaireComponent,
    UpdateQuestionnaireComponent,
    ViewQuestionnaireComponent,
    GitAllUserComponent,
    ViewRecordComponent,
    AddVaccineEmpComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpModule,
    FormsModule,
    NgxPaginationModule,
    ChartsModule ,
    
  ],
  providers: [
    OperationsService,
    AuthGuardAdminService,
    AuthService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
