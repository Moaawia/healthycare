import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardAdminService } from './service/auth-guard-admin.service';
import { AuthGuardEmpService } from './service/auth-guard-emp.service';
import { AuthGuardResponsibleService } from './service/auth-guard-responsible.service';
import { AddRecordComponent } from './system/employee/emp/operations/add-record/add-record.component';
import { AddVaccineEmpComponent } from './system/employee/emp/operations/add-vaccine-emp/add-vaccine-emp.component';
import { EditeRecordComponent } from './system/employee/emp/operations/edite-record/edite-record.component';
import { GitAllUserComponent } from './system/employee/emp/operations/git-all-user/git-all-user.component';
import { GitUserInfoComponent } from './system/employee/emp/operations/git-user-info/git-user-info.component';

import { MainOperationsEmpComponent } from './system/employee/emp/operations/main-operations-emp/main-operations-emp.component';
import { RegisterUserComponent } from './system/employee/emp/operations/register-user/register-user.component';
import { UpdateUserInfoEmpComponent } from './system/employee/emp/operations/update-user-info-emp/update-user-info-emp.component';
import { ViewRecordComponent } from './system/employee/emp/operations/view-record/view-record.component';

import { BlockUserComponent } from './system/employee/manager/operations/block-user/block-user.component';
import { DeleteUserComponent } from './system/employee/manager/operations/delete-user/delete-user.component';
import { GetUserComponent } from './system/employee/manager/operations/get-user/get-user.component';
import { GetUsersComponent } from './system/employee/manager/operations/get-users/get-users.component';


import { MainOperationsManagerComponent } from './system/employee/manager/operations/main-operations-manager/main-operations-manager.component';
import { RegisterEmpComponent } from './system/employee/manager/operations/register-emp/register-emp.component';
import { RegisterResponsibleComponent } from './system/employee/manager/operations/register-responsible/register-responsible.component';
import { UpdateUserInfoComponent } from './system/employee/manager/operations/update-user-info/update-user-info.component';
import { AddArticlesComponent } from './system/employee/responsible/operations/add-articles/add-articles.component';
import { AddDoctorComponent } from './system/employee/responsible/operations/add-doctor/add-doctor.component';
import { AddQuestionnaireComponent } from './system/employee/responsible/operations/add-questionnaire/add-questionnaire.component';
import { AddVaccineComponent } from './system/employee/responsible/operations/add-vaccine/add-vaccine.component';
import { DeleteArticlesComponent } from './system/employee/responsible/operations/delete-articles/delete-articles.component';
import { DeleteDoctorComponent } from './system/employee/responsible/operations/delete-doctor/delete-doctor.component';
import { EditArticlesComponent } from './system/employee/responsible/operations/edit-articles/edit-articles.component';
import { EditDoctorComponent } from './system/employee/responsible/operations/edit-doctor/edit-doctor.component';
import { GetAllDoctorComponent } from './system/employee/responsible/operations/get-all-doctor/get-all-doctor.component';
import { GetUserInfoComponent } from './system/employee/responsible/operations/get-user-info/get-user-info.component';

import { MainOperationsResponsibleComponent } from './system/employee/responsible/operations/main-operations-responsible/main-operations-responsible.component';
import { ManageArticalesOperationsComponent } from './system/employee/responsible/operations/manage-articales-operations/manage-articales-operations.component';
import { ManageArticalesComponent } from './system/employee/responsible/operations/manage-articales/manage-articales.component';
import { ManagerDoctorComponent } from './system/employee/responsible/operations/manager-doctor/manager-doctor.component';
import { ManagerQuestionnaireComponent } from './system/employee/responsible/operations/manager-questionnaire/manager-questionnaire.component';
import { ShowStatisticsComponent } from './system/employee/responsible/operations/show-statistics/show-statistics.component';
import { UpdateQuestionnaireComponent } from './system/employee/responsible/operations/update-questionnaire/update-questionnaire.component';
import { UpdateUserInfoResComponent } from './system/employee/responsible/operations/update-user-info-res/update-user-info-res.component';
import { ViewArticleComponent } from './system/employee/responsible/operations/view-article/view-article.component';
import { ViewQuestionnaireComponent } from './system/employee/responsible/operations/view-questionnaire/view-questionnaire.component';
import { ViewResultComponent } from './system/employee/responsible/operations/view-result/view-result.component';
import { HomeComponent } from './system/home/home.component';
import { LoginComponent } from './system/login/login.component';





const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'system/employee/responsible/operations/main-operations-responsible', component: MainOperationsResponsibleComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/emp/operations/main-operations-emp', component: MainOperationsEmpComponent, canActivate: [AuthGuardEmpService] },
  { path: 'system/employee/manager/operations/main-operations-manager', component: MainOperationsManagerComponent, canActivate: [AuthGuardAdminService] },
  { path: 'system/login', component: LoginComponent },
  // routing to responsible folder
  { path: 'system/employee/responsible/operations/show-statistics', component: ShowStatisticsComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/view-result', component: ViewResultComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/add-vaccine', component: AddVaccineComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/add-questionnaire', component: AddQuestionnaireComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/manage-articales', component: ManageArticalesComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/view-article', component: ViewArticleComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/manage-articales-operations', component: ManageArticalesOperationsComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/add-articles', component: AddArticlesComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/delete-articles', component: DeleteArticlesComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/edit-articles', component: EditArticlesComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/get-all-doctor', component: GetAllDoctorComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/manager-doctor', component: ManagerDoctorComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/add-doctor', component: AddDoctorComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/delete-doctor', component: DeleteDoctorComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/edit-doctor', component: EditDoctorComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/get-user-info', component: GetUserInfoComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/update-user-info-res', component: UpdateUserInfoResComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/manager-questionnaire', component: ManagerQuestionnaireComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/update-questionnaire', component: UpdateQuestionnaireComponent, canActivate: [AuthGuardResponsibleService] },
  { path: 'system/employee/responsible/operations/view-questionnaire', component: ViewQuestionnaireComponent, canActivate: [AuthGuardResponsibleService] },
  // routing to emp folder
  { path: 'system/employee/emp/operations/add-record', component: AddRecordComponent, canActivate: [AuthGuardEmpService] },
  { path: 'system/employee/emp/operations/edite-record', component: EditeRecordComponent, canActivate: [AuthGuardEmpService] },
  { path: 'system/employee/emp/operations/register-user', component: RegisterUserComponent, canActivate: [AuthGuardEmpService] },
  { path: 'system/employee/emp/operations/git-user-info', component: GitUserInfoComponent, canActivate: [AuthGuardEmpService] },
  { path: 'system/employee/emp/operations/update-user-info-emp', component: UpdateUserInfoEmpComponent, canActivate: [AuthGuardEmpService] },
  { path: 'system/employee/emp/operations/git-all-user', component: GitAllUserComponent, canActivate: [AuthGuardEmpService] },
  {path:  'system/employee/emp/operations/view-record',component:ViewRecordComponent,canActivate: [AuthGuardEmpService]},
  {path:'system/employee/emp/operations/add-vaccine-emp',component:AddVaccineEmpComponent,canActivate: [AuthGuardEmpService]},
  // routing to manager folder

  { path: 'system/employee/manager/operations/block-user', component: BlockUserComponent, canActivate: [AuthGuardAdminService] },
  { path: 'system/employee/manager/operations/register-responsible', component: RegisterResponsibleComponent, canActivate: [AuthGuardAdminService] },
  { path: 'system/employee/manager/operations/register-emp', component: RegisterEmpComponent, canActivate: [AuthGuardAdminService] },
  { path: 'system/employee/manager/operations/get-user', component: GetUserComponent, canActivate: [AuthGuardAdminService] },
  { path: 'system/employee/manager/operations/get-users', component: GetUsersComponent, canActivate: [AuthGuardAdminService] },
  { path: 'system/employee/manager/operations/delete-user', component: DeleteUserComponent, canActivate: [AuthGuardAdminService] },
  { path: 'system/employee/manager/operations/update-user-info', component: UpdateUserInfoComponent, canActivate: [AuthGuardAdminService] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
