import { Component, OnInit } from '@angular/core';

import { AuthService } from 'src/app/service/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
 
  constructor(public service:AuthService) { }
 
  logIn() {
  this.service.logIn();
    
  }
  processResults(obj: any) {
    return this.service.processResults(obj); 
  }

}
