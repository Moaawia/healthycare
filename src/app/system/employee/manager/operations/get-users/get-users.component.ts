import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import {catchError, map} from 'rxjs/operators';
import {throwError} from 'rxjs';
@Component({
  selector: 'app-get-users',
  templateUrl: './get-users.component.html',
  styleUrls: ['./get-users.component.css']
})
//coomment
export class GetUsersComponent implements OnInit {
  users: any={};
  userId:any={};
  totalRecords:number=1;
  page:number=1;
  constructor(private http: Http) { }
  url: string = 'http://localhost:3001/api/user';

  ngOnInit(): void {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    console.log('page1'+this.page)
    
    this.http.get(this.url+'?page='+(this.page-1),{ headers }).subscribe(response => {
      console.log('page2'+this.page)
      console.log(response.json());
      this.users = response.json();
      this.totalRecords=response.json().totalDocs;
      console.log('totalRecords'+this.totalRecords);
    })
  } 
  pageChanged($event:any){
    console.log($event);
    this.page=$event;
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    console.log('page3'+this.page)
    this.http.get(this.url+'?page='+(this.page-1),{ headers }).subscribe(response => {
      console.log('page4'+this.page)
      console.log(response.json());
      this.users = response.json();
      this.totalRecords=response.json().totalDocs;
      console.log('totalRecords'+this.totalRecords);
    })
  }
  getUserId(Id:any){
    console.log(Id);
    let headers = new Headers({ 'Content-Type': 'application/json','Authorization':'Bearer '+ localStorage.getItem('token') });
 
    this.http.get(this.url+'/'+ Id._id,{ headers }).subscribe(response => {
      console.log('res\n'+response.json());
      this.userId = response.json();
      console.log('userid\n'+this.userId)

    })
  }
  deleteUser(deluser:any) {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    // var index = Object.keys(this.users).indexOf(deluser);
     let index =this.users.docs.indexOf(deluser);
    console.log(index);
    this.http.delete(this.url + '/' + deluser._id,{headers}).subscribe(response => { (this.users.docs.splice(index, 1)); })
  }
  blockUser(Id: any) {
   
   
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    let index = this.users.docs.indexOf(Id);
    this.http.put(this.url + '/' + Id._id,{},{ headers }).subscribe(response => {
        if(this.users.docs[index].isBlocked == 'true')
       { this.users.docs[index].isBlocked = 'false';
     
      }
        else this.users.docs[index].isBlocked = 'true';
        document.getElementById("demo")?.innerHTML;
    })
  }

}
