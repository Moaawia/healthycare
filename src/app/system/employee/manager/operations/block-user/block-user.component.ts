import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
@Component({
  selector: 'app-block-user',
  templateUrl: './block-user.component.html',
  styleUrls: ['./block-user.component.css']
})
export class BlockUserComponent implements OnInit {
  users: any = {};
  constructor(private http: Http) { }
  url: string = 'http://localhost:3001/api/user';

  ngOnInit(): void {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    this.http.get(this.url, { headers }).subscribe(response => {
      console.log(response.json());
      this.users = response.json();
    })
  }
  blockUser(Id: any) {
 
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    let index = this.users.docs.indexOf(Id);
    this.http.put(this.url + '/' + Id._id,{},{ headers }).subscribe(response => {
      console.log(this.users.docs);
        // this.users.docs[index].isBlocked = true;

    })
  }

}
