import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http'
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-register-emp',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">مرحباً!</h4>
    </div>
    <div class="modal-body">
      <p> {{name}}!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark"  (click)="activeModal.close('Close click')">إغلاق</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() name: any;

  constructor(public activeModal: NgbActiveModal) {}
}
@Component({
  selector: 'app-register-emp',
  templateUrl: './register-emp.component.html',
  styleUrls: ['./register-emp.component.css']
})
export class RegisterEmpComponent implements OnInit {

  empObj =
  {
    username: '',
    firstName: '',
    lastName: '',
    password: '',
    birthDate:'',
    phoneNumber:'',
    email:'',
    fatherName:'NAN',
    motherName:'NAN',
  }
  userinfo: any = {};
  urlinfo: string = 'http://localhost:3001/api/user/profile';
  url: string ='http://localhost:3001/api/user/register-receptionist'
constructor(private route:Router, private http:Http,private modalService: NgbModal) { }

ngOnInit(): void {
  let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
  this.http.get(this.urlinfo, { headers }).subscribe(response => {
    console.log('res\n' + JSON.parse((response as any)._body));
    this.userinfo = response.json();
  })
}
registerEmployee() {
  console.log(this.empObj.fatherName)
  let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization':'Bearer '+localStorage.getItem('token')});
  this.http.post(this.url, this.empObj, { headers }).subscribe(response => { 
    if(response.ok){
      console.log(response.json.toString);
      this.route.navigate(['/system/employee/manager/operations/main-operations-manager']);
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.name = 'تمت عملية التسجيل بنجاح';

    }else{
      console.log(response.json.toString);
     
    }
   },
    err => {
      console.log('error   '+err)
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.name = 'فشلت عملية التسجيل';
    });
  console.log(this.empObj);


}}


