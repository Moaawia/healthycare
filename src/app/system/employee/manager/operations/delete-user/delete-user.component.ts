import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
@Component({
  selector: 'app-delete-user',
  templateUrl: './delete-user.component.html',
  styleUrls: ['./delete-user.component.css']
})
export class DeleteUserComponent implements OnInit {
  users: any={};
  constructor(private http: Http) { }
  url: string = 'http://localhost:3001/api/user';
  ngOnInit(): void {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.get(this.url,{ headers }).subscribe(response => {
      console.log(response.json());
      this.users = response.json();
    })
  }
  deleteUser(deluser:any) {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    // var index = Object.keys(this.users).indexOf(deluser);
     let index =this.users.docs.indexOf(deluser);
    console.log(index);
    this.http.delete(this.url + '/' + deluser._id,{headers}).subscribe(response => { (this.users.docs.splice(index, 1)); })
  }

}
