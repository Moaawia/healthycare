import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainOperationsManagerComponent } from './main-operations-manager.component';

describe('MainOperationsManagerComponent', () => {
  let component: MainOperationsManagerComponent;
  let fixture: ComponentFixture<MainOperationsManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainOperationsManagerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainOperationsManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
});
