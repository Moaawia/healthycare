import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';

@Component({
  selector: 'app-main-operations-manager',
  templateUrl: './main-operations-manager.component.html',
  styleUrls: ['./main-operations-manager.component.css']
})
export class MainOperationsManagerComponent implements OnInit {

  userinfo: any = {};
  constructor(private http: Http) { }
  url: string = 'http://localhost:3001/api/user/profile';

  ngOnInit(): void {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    this.http.get(this.url, { headers }).subscribe(response => {
      console.log('res\n' + JSON.parse((response as any)._body));
      this.userinfo = response.json();
    })

  }

}
