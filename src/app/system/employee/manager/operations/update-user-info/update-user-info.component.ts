import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-update-user-info',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Hi</h4>
    </div>
    <div class="modal-body">
      <p>{{name}}</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark"  (click)="activeModal.close('Close click')">إغلاق</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() name: any;

  constructor(public activeModal: NgbActiveModal) {}
}
@Component({
  selector: 'app-update-user-info',
  templateUrl: './update-user-info.component.html',
  styleUrls: ['./update-user-info.component.css']
})
export class UpdateUserInfoComponent implements OnInit {
  updateuserinfo = {
    firstName: '',
    lastName: '',
    username:'',
    email:'',
    phoneNumber:'',
    motherName:'',
    birthDate:'',
    fatherName:'',
  }
  users:any={};
  constructor(private http: Http,private modalService: NgbModal) { }
  url: string = 'http://localhost:3001/api/user/profile';
  ngOnInit(): void {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.get(this.url,{ headers }).subscribe(response => {
      console.log('res\n')
      console.log(JSON.parse((response as any)._body));
      this.users = response.json();
    },
    err => {
      console.log('error   '+err)
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.name = 'لم ينجح التعديل';
    }
    )
  }
  updateUserInfo() {


    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    this.http.put(this.url, this.updateuserinfo, { headers }).subscribe(response => {

    })
    const modalRef = this.modalService.open(NgbdModalContent);
    modalRef.componentInstance.name = 'تمت عملية التعديل بنجاح';
  }

}
