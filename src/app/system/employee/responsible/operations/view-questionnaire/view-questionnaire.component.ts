import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { ActivatedRoute }     from '@angular/router';
@Component({
  selector: 'app-view-questionnaire',
  templateUrl: './view-questionnaire.component.html',
  styleUrls: ['./view-questionnaire.component.css']
})
export class ViewQuestionnaireComponent implements OnInit {

  question:any[]=[];
  questionObj:any={};
  index:any;
   constructor(private route: ActivatedRoute,private http: Http) { }
   url: string = 'http://localhost:3001/api/questionnaire';
 
   ngOnInit(): void {
      let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
     this.http.get(this.url,{ headers }).subscribe(response => {
       console.log(response.json());
       this.question = response.json();
       console.log( this.question);
       this.index=this.route.snapshot.paramMap.get('id');
       console.log('index:  '+this.index)  
     this.questionObj =this.question[this.index];
     console.log('questionObj')
     console.log(this.questionObj)
     })
 
   }

}
