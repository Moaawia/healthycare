import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-add-doctor',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">مرحبا!</h4>
    </div>
    <div class="modal-body">
      <p> {{name}}!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark"  (click)="activeModal.close('Close click')">إغلاق</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() name: any;

  constructor(public activeModal: NgbActiveModal) {}
}

@Component({
  selector: 'app-add-doctor',
  templateUrl: './add-doctor.component.html',
  styleUrls: ['./add-doctor.component.css']
})
export class AddDoctorComponent implements OnInit {
  doctorObj =
  {
    name:'',
    specialisation:'',
    city:'',
    locationDesc:'',
    longitude:'',
    latitude:'',
    openTime:'',
    closeTime:'',
  }
  url: string ='http://localhost:3001/api/doctor'
  constructor(private route:Router, private http:Http,private modalService: NgbModal) { }

  ngOnInit(): void {
  }
  addDoctor() {

    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.post(this.url, this.doctorObj, { headers }).subscribe(response => { 
      if(response.ok){
        console.log(response.json.toString);
        this.route.navigate(['/system/employee/responsible/operations/manager-doctor']);
        const modalRef = this.modalService.open(NgbdModalContent);
        modalRef.componentInstance.name = 'تمت عملية الإظافة';
        // this.route.navigate(['/system/employee/manager/operations/main-operations-manager']);
      }else{
        console.log(response.json.toString);
      }
     },
     err => {
      console.log(err)
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.name = 'فشلت عملية الإظافة';
    });
    console.log(this.doctorObj);

  
  }
}
