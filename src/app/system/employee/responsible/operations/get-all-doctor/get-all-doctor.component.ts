import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
@Component({
  selector: 'app-get-all-doctor',
  templateUrl: './get-all-doctor.component.html',
  styleUrls: ['./get-all-doctor.component.css']
})
export class GetAllDoctorComponent implements OnInit {

  doctor: any[]=[];
  constructor(private http: Http) { }
  url: string = 'http://localhost:3001/api/doctor';

  ngOnInit(): void {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.get(this.url,{ headers }).subscribe(response => {
      console.log(response.json());
      this.doctor = response.json();
    })
  } 

}
