import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { ActivatedRoute }     from '@angular/router';
@Component({
  selector: 'app-view-article',
  templateUrl: './view-article.component.html',
  styleUrls: ['./view-article.component.css']
})
export class ViewArticleComponent implements OnInit {
 Articles:any[]=[];
 ArticleObj:any={};
 index:any;
  constructor(private route: ActivatedRoute,private http: Http) { }
  url: string = 'http://localhost:3001/api/article';

  ngOnInit(): void {
     let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.get(this.url,{ headers }).subscribe(response => {
      console.log(response.json());
      this.Articles = response.json();
      console.log( this.Articles);
      this.index=this.route.snapshot.paramMap.get('id');
      console.log('index:  '+this.index)  
    this.ArticleObj =this.Articles[this.index];
    console.log('ArticleObj')
    console.log(this.ArticleObj)
    })

  }

}
