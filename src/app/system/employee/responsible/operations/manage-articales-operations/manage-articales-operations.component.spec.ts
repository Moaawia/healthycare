import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageArticalesOperationsComponent } from './manage-articales-operations.component';

describe('ManageArticalesOperationsComponent', () => {
  let component: ManageArticalesOperationsComponent;
  let fixture: ComponentFixture<ManageArticalesOperationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageArticalesOperationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageArticalesOperationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
