import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUserInfoResComponent } from './update-user-info-res.component';

describe('UpdateUserInfoResComponent', () => {
  let component: UpdateUserInfoResComponent;
  let fixture: ComponentFixture<UpdateUserInfoResComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateUserInfoResComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUserInfoResComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
