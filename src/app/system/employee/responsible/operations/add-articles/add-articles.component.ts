import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Headers } from '@angular/http';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-add-articles',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">مرحباً!</h4>
    </div>
    <div class="modal-body">
      <p>{{name}}!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark"  (click)="activeModal.close('Close click')">إغلاق</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() name: any;

  constructor(public activeModal: NgbActiveModal) {}
}
@Component({
  selector: 'app-add-articles',
  templateUrl: './add-articles.component.html',
  styleUrls: ['./add-articles.component.css']
})
export class AddArticlesComponent implements OnInit {
  // post: any[] = [];
  // constructor(private http: Http) { }
  // url: string = 'https://jsonplaceholder.typicode.com/posts';
  // ngOnInit(): void {
  //   this.http.get(this.url).subscribe(response => {
  //     this.post = response.json()
  //   })
  // }
  // creatArticle(input:HTMLInputElement){
  //   let creat={title:input.value,id:''}
  //   this.http.post(this.url,JSON.stringify(creat)).subscribe(response=>{
  //     this.post.splice(0,0,creat);
     
  //   })
  // }
  articleObj =
  {
    title:'',
    content:'',
    topics:'',
  }
  url: string ='http://localhost:3001/api/article'
  constructor(private route:Router, private http:Http,private modalService: NgbModal) { }

  ngOnInit(): void {
  }
  addArticle() {

    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.post(this.url, this.articleObj, { headers }).subscribe(response => { 
      if(response.ok){
        console.log(response.json.toString);
            this.route.navigate(['/system/employee/responsible/operations/manage-articales']);
        const modalRef = this.modalService.open(NgbdModalContent);
        modalRef.componentInstance.name = 'تمت عملية الإظافة';
        // this.route.navigate(['/system/employee/manager/operations/main-operations-manager']);
      }else{
        console.log(response.json.toString);
      }
     },
    err => {
      console.log('error   '+err)
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.name = 'فشلت عملية الإظافة';
    });
    console.log(this.articleObj);

  
  }

}
