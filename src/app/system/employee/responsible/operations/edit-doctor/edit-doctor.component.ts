import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { ActivatedRoute } from '@angular/router';
import { ParamMap, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-edit-doctor',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">مرحباً!</h4>
    </div>
    <div class="modal-body">
      <p>{{name}}!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark"  (click)="activeModal.close('Close click')">إغلاق</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() name: any;

  constructor(public activeModal: NgbActiveModal) {}
}
@Component({
  selector: 'app-edit-doctor',
  templateUrl: './edit-doctor.component.html',
  styleUrls: ['./edit-doctor.component.css']
})
export class EditDoctorComponent implements OnInit {

  editDoctorObj =
    {
      name: '',
      specialisation: '',
      city: '',
      locationDesc: '',
      longitude: '',
      latitude: '',
      openTime: '',
      closeTime: '',
    }
  doctor: any[] = [];
  doctors: any[] = [];
  doctorObj: any = {};
  

  index: any;
  constructor(private route: ActivatedRoute,private route1:Router ,private http: Http,private modalService: NgbModal) { }
  url: string = 'http://localhost:3001/api/doctor';

  ngOnInit(): void {

    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    this.http.get(this.url, { headers }).subscribe(response => {
      console.log(response.json());
      this.doctors = response.json();
      console.log(this.doctors);
      this.index = this.route.snapshot.paramMap.get('id');
      console.log('index:  ' + this.index)
      this.doctorObj = this.doctors[this.index];
    
      console.log('doctorObj ')
      console.log(this.doctorObj)

    })
    //  this.index=this.route.snapshot.paramMap.get('id');
    // this.doctorObj =this.doctors[this.index];
    // this.http.get(this.url+'/'+this.doctorObj._id,{ headers }).subscribe(response => {
    //   console.log(response.json());
    //   this.doctor = response.json();
    //   console.log( this.doctor);
    // })


  }

  editDoctorInfo() {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    this.http.put(this.url + '/' + this.doctorObj._id, this.editDoctorObj, { headers }).subscribe(response => {
      if(response.ok)
      {
        this.route1.navigate(['/system/employee/responsible/operations/manager-doctor']);
        const modalRef = this.modalService.open(NgbdModalContent);
        modalRef.componentInstance.name = 'تمت عملية التعديل';
      }
    },
    err => {
     console.log(err)
     const modalRef = this.modalService.open(NgbdModalContent);
     modalRef.componentInstance.name = 'فشل التعديل';
   });
  }
}
