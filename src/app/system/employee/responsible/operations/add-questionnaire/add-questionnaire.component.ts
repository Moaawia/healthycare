import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-questionnaire',
  templateUrl: './add-questionnaire.component.html',
  styleUrls: ['./add-questionnaire.component.css']
})
export class AddQuestionnaireComponent implements OnInit {
  questionObj =
  {
    categories:'',
    title:'',
    description:'',
    topic:'',
    ageRange:'',
    questions:[{}],
    results:[{}],
  
  }
  obj1= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj2= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj3= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj4= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj5= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj6= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj7= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj8= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj9= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj10= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj11= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj12= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj13= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj14= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj15= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj16= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj17= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj18= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj19= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
    obj20= {
    question: '',
    choices: ['نعم', 'لا'],
    correctAnswer: '',
  }
  res1= {
    min: '',
    max: '',
    result:'',
  }
  res2= {
    min: '',
    max: '',
    result:'',
  }
 
  url: string ='http://localhost:3001/api/questionnaire'
  constructor(private route:Router, private http:Http) { }

  ngOnInit(): void {
  }
  addQuestionnaire() {
 console.log('categories'+this.questionObj.categories)
 console.log('question'+this.obj1.question)
this.questionObj.questions.push(this.obj1)
this.questionObj.questions.push(this.obj2)
this.questionObj.questions.push(this.obj3)
this.questionObj.questions.push(this.obj4)
this.questionObj.questions.push(this.obj5)
this.questionObj.questions.push(this.obj6)
this.questionObj.questions.push(this.obj7)
this.questionObj.questions.push(this.obj8)
this.questionObj.questions.push(this.obj9)
this.questionObj.questions.push(this.obj10)
this.questionObj.questions.push(this.obj11)
this.questionObj.questions.push(this.obj12)
this.questionObj.questions.push(this.obj13)
this.questionObj.questions.push(this.obj14)
this.questionObj.questions.push(this.obj15)
this.questionObj.questions.push(this.obj16)
this.questionObj.questions.push(this.obj17)
this.questionObj.questions.push(this.obj18)
this.questionObj.questions.push(this.obj19)
this.questionObj.questions.push(this.obj20)

this.questionObj.results.push(this.res1)
this.questionObj.results.push(this.res2)

console.log('questions'+this.questionObj.questions)
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.post(this.url, this.questionObj, { headers }).subscribe(response => { 
      if(response.ok){
        console.log(response.json.toString);

        // this.route.navigate(['/system/employee/manager/operations/main-operations-manager']);
      }else{
        console.log(response.json.toString);
      }
     },
     err => {
      console.log(err)
    });
    console.log('questionObj')
    console.log(this.questionObj);

  
  }
}
