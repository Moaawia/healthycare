import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
@Component({
  selector: 'app-delete-articles',
  templateUrl: './delete-articles.component.html',
  styleUrls: ['./delete-articles.component.css']
})
export class DeleteArticlesComponent implements OnInit {
  post: any[] = [];
  constructor(private http: Http) { }
  url: string = 'https://jsonplaceholder.typicode.com/posts';
  ngOnInit(): void {
    this.http.get(this.url).subscribe(response => {
      this.post = response.json()
    })
  }
  deleteArticle(articale: any) {
    let  index = this.post.indexOf(articale);
      this.http.delete(this.url + '/' + articale.id).subscribe(ressponse => {
        this.post.splice(index, 1);
      })
    }

}
