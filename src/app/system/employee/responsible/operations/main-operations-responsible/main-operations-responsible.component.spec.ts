import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainOperationsResponsibleComponent } from './main-operations-responsible.component';

describe('MainOperationsResponsibleComponent', () => {
  let component: MainOperationsResponsibleComponent;
  let fixture: ComponentFixture<MainOperationsResponsibleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainOperationsResponsibleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainOperationsResponsibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
