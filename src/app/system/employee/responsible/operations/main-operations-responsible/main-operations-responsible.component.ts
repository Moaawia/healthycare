import { Component, OnInit } from '@angular/core';
import '@angular/localize/init';

@Component({
  selector: 'app-main-operations-responsible',
  templateUrl: './main-operations-responsible.component.html',
  styleUrls: ['./main-operations-responsible.component.css']
})
export class MainOperationsResponsibleComponent implements OnInit {
username:any;
  constructor() { }

  ngOnInit(): void {

  }
  images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/1116/524`);
}
