import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Headers } from '@angular/http';


@Component({
  selector: 'app-manage-articales',
  templateUrl: './manage-articales.component.html',
  styleUrls: ['./manage-articales.component.css']
})
export class ManageArticalesComponent implements OnInit {
  
  articles: any[]=[];
  A: any;
  B: any;
  C: any;
  constructor(private route:Router,private http: Http) { }
  url: string = 'http://localhost:3001/api/article';
  ngOnInit(): void {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.get(this.url,{ headers }).subscribe(response => {
      console.log(response.json());
      this.articles = response.json();
  this.A=this.articles.filter(item=>item.topics=='قسم التغذية')
  console.log('A');
  console.log(this.A);
  this.B=this.articles.filter(item=>item.topics=='الصحة النفسية')
  console.log('B');
  console.log(this.B);
  this.C=this.articles.filter(item=>item.topics=='قسم النمو')
  console.log('C');
  console.log(this.C);

    })  
  }
  deleteArticle(delArticle:any) {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    // var index = Object.keys(this.users).indexOf(deluser);
     let index =this.articles.indexOf(delArticle);
    console.log(index);
    this.http.delete(this.url + '/' + delArticle._id,{headers}).subscribe(response => { (this.articles.splice(index, 1)); })
  } 
edit(id:any){
  let i=this.articles.indexOf(id);
  this.route.navigate( ["/system/employee/responsible/operations/edit-articles", { id: i}]);
}
view(id:any){
  let i=this.articles.indexOf(id);
  this.route.navigate( ["/system/employee/responsible/operations/view-article", { id: i}]);
}

}




