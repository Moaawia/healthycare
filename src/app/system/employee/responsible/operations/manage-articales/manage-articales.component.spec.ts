import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageArticalesComponent } from './manage-articales.component';

describe('ManageArticalesComponent', () => {
  let component: ManageArticalesComponent;
  let fixture: ComponentFixture<ManageArticalesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManageArticalesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageArticalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
