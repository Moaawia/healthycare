import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerQuestionnaireComponent } from './manager-questionnaire.component';

describe('ManagerQuestionnaireComponent', () => {
  let component: ManagerQuestionnaireComponent;
  let fixture: ComponentFixture<ManagerQuestionnaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagerQuestionnaireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerQuestionnaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
