import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-manager-questionnaire',
  templateUrl: './manager-questionnaire.component.html',
  styleUrls: ['./manager-questionnaire.component.css']
})
export class ManagerQuestionnaireComponent implements OnInit {

  questionnaire: any[]=[]
  constructor(private route:Router,private http: Http) { }
  url: string = 'http://localhost:3001/api/questionnaire';

  ngOnInit(): void {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.get(this.url,{ headers }).subscribe(response => {
      console.log(response.json());
      this.questionnaire = response.json();
    })
  }
  view(id:any){
    let i=this.questionnaire.indexOf(id);
    this.route.navigate( ["/system/employee/responsible/operations/view-questionnaire", { id: i}]);
  }
}
