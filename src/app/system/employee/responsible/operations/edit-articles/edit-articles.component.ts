import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { ActivatedRoute ,Router}     from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-add-articles',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">مرحباً!</h4>
    </div>
    <div class="modal-body">
      <p>{{name}}!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark"  (click)="activeModal.close('Close click')">إغلاق</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() name: any;

  constructor(public activeModal: NgbActiveModal) {}
}
@Component({
  selector: 'app-edit-articles',
  templateUrl: './edit-articles.component.html',
  styleUrls: ['./edit-articles.component.css']
})
export class EditArticlesComponent implements OnInit {
  // post: any[] = [];
  // constructor(private http: Http) { }
  // url: string = 'https://jsonplaceholder.typicode.com/posts';
  // ngOnInit(): void {
  //   this.http.get(this.url).subscribe(response => {
  //     this.post = response.json()
  //   })
  // }
  // updateArticle(articale:any,inputarticale:any){
  //   let update={title:inputarticale,id:articale.id}
  //   this.http.put(this.url+'/'+articale.id,articale.id,JSON.stringify(update)).subscribe(response=>{
  //     let index=this.post.indexOf(articale);
  //     this.post[index]=update;
  //   })
  // }
  editArticalObj =
  {
    title:'',
    content:'',
    topics:'',
  }
  Article: any[]=[];
  Articles: any[]=[];
  ArticleObj:any={};
  index:any;
  constructor(private route: ActivatedRoute,private route1:Router,private http: Http,private modalService: NgbModal) { }
  url: string = 'http://localhost:3001/api/article';

  ngOnInit(): void {
    
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.get(this.url,{ headers }).subscribe(response => {
      console.log(response.json());
      this.Articles = response.json();
      console.log( this.Articles);
      this.index=this.route.snapshot.paramMap.get('id');
      console.log('index:  '+this.index)  
    this.ArticleObj =this.Articles[this.index];
    console.log(this.ArticleObj)
    })
    //  this.index=this.route.snapshot.paramMap.get('id');
    // this.doctorObj =this.doctors[this.index];
    // this.http.get(this.url+'/'+this.doctorObj._id,{ headers }).subscribe(response => {
    //   console.log(response.json());
    //   this.doctor = response.json();
    //   console.log( this.doctor);
    // })
    
                                       
  }
  
  editArticleInfo() {
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    this.http.put(this.url+'/'+this.ArticleObj._id, this.editArticalObj, { headers }).subscribe(response => {
if(response.ok){
    this.route1.navigate(['/system/employee/responsible/operations/manage-articales']);
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.name = 'نجح التعديل';
}
    },
    err => {
      console.log('error   '+err)
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.name = 'فشل التعديل';
    });
  }
}
