import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-manager-doctor',
  templateUrl: './manager-doctor.component.html',
  styleUrls: ['./manager-doctor.component.css']
})
export class ManagerDoctorComponent implements OnInit {

  doctor: any[]=[];
  constructor(private route:Router,private http: Http) { }
  url: string = 'http://localhost:3001/api/doctor';

  ngOnInit(): void {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.get(this.url,{ headers }).subscribe(response => {
      console.log(response.json());
      this.doctor = response.json();
    })
  }
  deleteDoctor(delDoctor:any) {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    // var index = Object.keys(this.users).indexOf(deluser);
     let index =this.doctor.indexOf(delDoctor);
    console.log(index);
    this.http.delete(this.url + '/' + delDoctor._id,{headers}).subscribe(response => { (this.doctor.splice(index, 1)); })
  } 
edit(id:any){
  let i=this.doctor.indexOf(id);
  this.route.navigate( ["/system/employee/responsible/operations/edit-doctor", { id: i}]);
}

}
