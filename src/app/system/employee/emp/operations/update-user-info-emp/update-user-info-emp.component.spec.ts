import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateUserInfoEmpComponent } from './update-user-info-emp.component';

describe('UpdateUserInfoEmpComponent', () => {
  let component: UpdateUserInfoEmpComponent;
  let fixture: ComponentFixture<UpdateUserInfoEmpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateUserInfoEmpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateUserInfoEmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
