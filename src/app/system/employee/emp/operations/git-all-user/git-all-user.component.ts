import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-git-all-user',
  templateUrl: './git-all-user.component.html',
  styleUrls: ['./git-all-user.component.css']
})
export class GitAllUserComponent implements OnInit {

  user: any={};
  totalRecords:number=1;
  page:number=1;
  constructor(private http: Http,private route:Router) { }
  url: string = 'http://localhost:3001/api/user';

  ngOnInit(): void {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.get(this.url+'?page='+(this.page-1),{ headers }).subscribe(response => {
      console.log('user');
      console.log(response.json());
      this.user = response.json();
    })
  } 
  pageChanged($event:any){
    console.log($event);
    this.page=$event;
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    console.log('page3'+this.page)
    this.http.get(this.url+'?page='+(this.page-1),{ headers }).subscribe(response => {
      console.log('page4'+this.page)
      console.log(response.json());
      this.user = response.json();
      this.totalRecords=response.json().totalDocs;
      console.log('totalRecords'+this.totalRecords);
    })
  }
  update(id:any){
    let i=this.user.docs.indexOf(id);
    let recordid=this.user.docs[i].record;
    this.route.navigate( ["/system/employee/emp/operations/edite-record", { record: recordid}]);
  }
  view(id:any){
    console.log('record id');
    console.log(this.user.docs);
    let i=this.user.docs.indexOf(id);
    let recordid=this.user.docs[i].record;
    console.log('record');
    console.log(recordid);
    this.route.navigate( ["/system/employee/emp/operations/view-record", { record: recordid}]);
  }

}
