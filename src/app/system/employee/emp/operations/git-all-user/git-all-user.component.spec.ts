import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GitAllUserComponent } from './git-all-user.component';

describe('GitAllUserComponent', () => {
  let component: GitAllUserComponent;
  let fixture: ComponentFixture<GitAllUserComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GitAllUserComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GitAllUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
