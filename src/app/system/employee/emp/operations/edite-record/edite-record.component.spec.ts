import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditeRecordComponent } from './edite-record.component';

describe('EditeRecordComponent', () => {
  let component: EditeRecordComponent;
  let fixture: ComponentFixture<EditeRecordComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditeRecordComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditeRecordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
