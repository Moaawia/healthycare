import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { ActivatedRoute ,Router}     from '@angular/router';
@Component({
  selector: 'app-edite-record',
  templateUrl: './edite-record.component.html',
  styleUrls: ['./edite-record.component.css']
})
export class EditeRecordComponent implements OnInit {

  editRecordObj =
  {
       gender:  '' ,
       currentMedications :  '' ,
       Familydisturbances : '',
       lonelyChild : '',
       ExposedToViolence : '',
       FinancialStausOfTheFamily :  '' ,
       GetHealthyFood : '',
       SleepRate :  '' ,
       playingRate :  '' ,
       mentalHealthIndicator :  '' ,
       educationLevelFather :  '' ,
       educationLevelmother : '' ,
       healthEducation : '' ,
       educationIndicatorForChild :  '' 
  }
  user: any={};
  userObj:any={};
  index:any;
  userrecord: any={};
  constructor(private route: ActivatedRoute,private route1:Router,private http: Http) { }
  url: string = 'http://localhost:3001/api/user';
  urlrecord: string = 'http://localhost:3001/api/record';

  ngOnInit(): void {
    
    // let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    // this.http.get(this.url,{ headers }).subscribe(response => {
    //   console.log(response.json());
    //   this.user = response.json();
    //   console.log( 'اليوزر تبع التعديل');
    //   console.log( this.user);
    //   this.index=this.route.snapshot.paramMap.get('id');
    //   console.log('indexxxxxxxx:  '+this.index)  
    // this.userObj =this.user.docs[this.index];
    // console.log('userobj')
    // console.log(this.userObj)
    // })
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.get(this.urlrecord+'/'+this.route.snapshot.paramMap.get('record'),{ headers }).subscribe(response => {
      console.log(response.json());
      this.userrecord = response.json();
      console.log('recorduser');
      console.log( this.userrecord);
    })
 
    
                                       
  }
  
  editRecord() {
    console.log(this.route.snapshot.paramMap.get('record'));
    let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
    this.http.put(this.urlrecord+'/'+this.route.snapshot.paramMap.get('record'), this.editRecordObj, { headers }).subscribe(response => {
if(response.ok){
    // this.route1.navigate(['/system/employee/responsible/operations/manage-articales']);
    //   const modalRef = this.modalService.open(NgbdModalContent);
    //   modalRef.componentInstance.name = 'Edit succeeded';
}
    },
    err => {
      console.log('error   '+err)
      // const modalRef = this.modalService.open(NgbdModalContent);
      // modalRef.componentInstance.name = 'Editing did not work';
    });
  }

}
