import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainOperationsEmpComponent } from './main-operations-emp.component';

describe('MainOperationsEmpComponent', () => {
  let component: MainOperationsEmpComponent;
  let fixture: ComponentFixture<MainOperationsEmpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainOperationsEmpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainOperationsEmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
