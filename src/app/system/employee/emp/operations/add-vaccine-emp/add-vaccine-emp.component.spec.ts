import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddVaccineEmpComponent } from './add-vaccine-emp.component';

describe('AddVaccineEmpComponent', () => {
  let component: AddVaccineEmpComponent;
  let fixture: ComponentFixture<AddVaccineEmpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddVaccineEmpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddVaccineEmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
