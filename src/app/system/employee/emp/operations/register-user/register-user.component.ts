import { Component, Input, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http'
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-register-emp',
  template: `
    <div class="modal-header">
      <h4 class="modal-title">Hi there!</h4>
    </div>
    <div class="modal-body">
      <p>Hello, {{name}}!</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-outline-dark"  (click)="activeModal.close('Close click')">Close</button>
    </div>
  `
})
export class NgbdModalContent {
  @Input() name: any;

  constructor(public activeModal: NgbActiveModal) {}
}
@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {
  userObj =
  {
    username: '',
    firstName: '',
    lastName: '',
    password: '',
    birthDate:'',
    phoneNumber:'',
    email:'',
    fatherName:'NAN',
    motherName:'NAN',
    record:{
      gender:  '' ,
       currentMedications :  '' ,
       Familydisturbances : '',
       lonelyChild : '',
       ExposedToViolence : '',
       FinancialStausOfTheFamily :  '' ,
       GetHealthyFood : '',
       SleepRate :  '' ,
       playingRate :  '' ,
       mentalHealthIndicator :  '' ,
       educationLevelFather :  '' ,
       educationLevelmother : '' ,
       healthEducation : '' ,
       educationIndicatorForChild :  '' 
      }
  }
  userinfo: any = {};
  urlinfo: string = 'http://localhost:3001/api/user/profile';
  url: string ='http://localhost:3001/api/user/register-user'
constructor(private route:Router, private http:Http,private modalService: NgbModal) { }

ngOnInit(): void {
  let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + localStorage.getItem('token') });
  this.http.get(this.urlinfo, { headers }).subscribe(response => {
    console.log('res\n' + JSON.parse((response as any)._body));
    this.userinfo = response.json();
    console.log(this.userinfo)
  })  
}
registerUser() {
  console.log(this.userObj.fatherName)
  let headers = new Headers({ 'Content-Type': 'application/json', 'Authorization':'Bearer '+localStorage.getItem('token')});
  this.http.post(this.url, this.userObj, { headers }).subscribe(response => { 
    if(response.ok){
      console.log(response.json.toString);
      this.route.navigate(['/system/employee/emp/operations/main-operations-emp']);
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.name = 'Registration succeeded';

    }else{
      console.log(response.json.toString);
     
    }
   },
    err => {
      console.log('error   '+err)
      const modalRef = this.modalService.open(NgbdModalContent);
      modalRef.componentInstance.name = 'Registration Unsucceeded';
    });
  console.log(this.userObj);
}

}
