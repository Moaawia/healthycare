import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { ActivatedRoute ,Router}     from '@angular/router';
@Component({
  selector: 'app-view-record',
  templateUrl: './view-record.component.html',
  styleUrls: ['./view-record.component.css']
})
export class ViewRecordComponent implements OnInit {
  url: string = 'http://localhost:3001/api/user';
  urlrecord: string = 'http://localhost:3001/api/record';
  user: any={};
  userrecord: any={};
  userObj:any={};
  index:any;
  constructor(private route: ActivatedRoute,private route1:Router,private http: Http) { }

  ngOnInit(): void {
    let headers = new Headers({'Content-Type': 'application/json',  'Authorization':'Bearer '+localStorage.getItem('token')});
    this.http.get(this.urlrecord+'/'+this.route.snapshot.paramMap.get('record'),{ headers }).subscribe(response => {
      console.log(response.json());
      this.userrecord = response.json();
      console.log('recorduser');
      console.log( this.userrecord);
    })
  }
}
