import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Router } from '@angular/router';
import { Headers } from '@angular/http';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  posts: any[] = [];
  logObj = {
    username: '',
    password: ''
  }
  user: any;
  usertoken:any;
  constructor(private route: Router, private http: Http) { }
  urlLogIn: string = 'http://localhost:3001/api/user/login';

  logIn() {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    this.http.post(this.urlLogIn, this.logObj, { headers }).subscribe(
      response => {
        if (response.ok) {

          localStorage.setItem('token', JSON.parse((response as any)._body).token);
          localStorage.setItem('role', JSON.parse((response as any)._body).role);
          this.user = localStorage.getItem('role');
          this.usertoken=localStorage.getItem('token')
          console.log(this.user);
          console.log(this.usertoken);
          // this.route.navigate(['/system/employee/manager/operations/main-operations-manager'||'/system/employee/responsible/operations/main-operations-responsible'||'/system/employee/emp/operations/main-operations-emp']);
          if (this.user == 'admin') {
            this.route.navigate(['/system/employee/manager/operations/main-operations-manager']);
          }
          else if (this.user == 'official') {
            this.route.navigate(['/system/employee/responsible/operations/main-operations-responsible']);
          }
          else if (this.user == 'receptionist') {
            this.route.navigate(['/system/employee/emp/operations/main-operations-emp']);
          }


        }
        else {
          console.log(response.json.toString);
        }
        console.log(response);
      },
      err => {
        console.log(err)
      }
    );
    console.log(this.logObj);

  }
  processResults(obj: any) {
    return JSON.parse(obj._body).token;
  }
}
