import { TestBed } from '@angular/core/testing';

import { AuthGuardEmpService } from './auth-guard-emp.service';

describe('AuthGuardEmpService', () => {
  let service: AuthGuardEmpService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthGuardEmpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
