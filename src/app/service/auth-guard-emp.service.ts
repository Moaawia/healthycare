import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardEmpService {

  constructor(private route: Router,private authService:AuthService) { }
  
  
  canActivate(){
    if(this.authService.user=='receptionist'){
      return true;
     
    } else
    {this.route.navigate(['']);
  return false;}
    
    
  }
}
