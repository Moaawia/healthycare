import { TestBed } from '@angular/core/testing';

import { AuthGuardResponsibleService } from './auth-guard-responsible.service';

describe('AuthGuardResponsibleService', () => {
  let service: AuthGuardResponsibleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthGuardResponsibleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
