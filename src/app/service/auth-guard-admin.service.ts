import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuardAdminService implements CanActivate {

  constructor(private route: Router,private authService:AuthService) { }
  
  
  canActivate(){
    if(this.authService.user=='admin'){
      return true;
     
    } else
    {this.route.navigate(['']);
  return false;}
    
    
  }
}
